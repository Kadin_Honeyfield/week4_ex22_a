﻿using System;
using System.Collections.Generic;

namespace week4_ex22_a
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new string[5] {"Harry Potter", "Pirates of the Carribean", "Step Brothers", "Game of Thrones", "The Avengers"};
            int counter = 5;
            int movieCount = movies.Length;
            int i = 0;

            //A
            Console.WriteLine("-------Ex A-------");
            for( i = 0; i < counter; i++)
            {
                Console.WriteLine(movies[i]);
            }
            
            //B
            Console.WriteLine("-------Ex B-------");
            movies[0] = "James Bond";
            movies[2] = "Bad Grampa";
            Console.WriteLine(string.Join(", ", movies));

            //C
            Console.WriteLine("-------Ex C-------");
            Array.Sort(movies);
            Console.WriteLine(string.Join(", ", movies));

            //D
            Console.WriteLine("-------Ex D-------");
            Console.WriteLine($"Movie Count: {movieCount}");

            //E
            Console.WriteLine("-------Ex E-------");
            var output = string.Join(", ", movies);
            Console.WriteLine(string.Join(", ", movies));

            //List<T>
            Console.WriteLine("-------List<T> Starts-------");

            var foodShops = new List<string> {"Pizza Library", "Lone Star", "Astro Lab", "Pap Tav", "Stars and Stripes"};
            
            //F
            Console.WriteLine("-------Ex F-------");
            Console.WriteLine(string.Join(", ", foodShops));

            //G
            Console.WriteLine("-------Ex G-------");
            foodShops.Sort();
            Console.WriteLine(string.Join(", ", foodShops));

            //H
            Console.WriteLine("-------Ex H-------");
            foodShops.Remove("Pap Tav");
            Console.WriteLine(string.Join(", ", foodShops));

        }
    }
}
